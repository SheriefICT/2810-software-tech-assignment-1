import unittest
import word_ladder.py

class testWordLadder(unittest.TestCase):

    def test_fileInputValidation(self):
        self.assertTrue(os.path.isfile("dictionary.txt"))
        self.assertFalse(vc.isVideoSrcValid("dictionary.txt"))

    def test_initialWordValidation(self):
        self.assertTrue(isNumeric(v))

    def test_targetWordValidation(self):
        self.assertIsInstance(val, int)

    @patch('yourmodule.get_input', return_value='yes')
    def test_ChoosePathOption_yes(self):
        self.assertEqual(answer(), 'you entered y')


    @patch('yourmodule.get_input', return_value='n')
    def test_ChoosePathOption_no(self):
        self.assertEqual(answer(), 'you entered n')



if __name__ == '__main__':
    unittest.main()
